<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_karyawan()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 200, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->KaryawanModel->check_auth_client();
            if ($check_auth_client == true) {
                $data = $this->KaryawanModel->get_all_karyawan();
                if (count($data) > 0) {
                    $result = [
                        'status' => 'success',
                        'result' => $data
                    ];
                } else {
                    $result = [
                        'status' => 'nodata',
                        'result' => null
                    ];
                }

                json_output(200, $result);
            }
        }
    }

    public function get_karyawan($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
            json_output(200, array('status' => 200, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->KaryawanModel->check_auth_client();
            if ($check_auth_client == true) {
                $data = $this->KaryawanModel->get_karyawan($id);
                json_output(200, $data);
            }
        }
    }

    public function insert_karyawan()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 200, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->KaryawanModel->check_auth_client();
            if ($check_auth_client == true) {
                $params = $_REQUEST;
                if ($params['nama'] == "" || $params['umur'] == "") {
                    $respStatus = 400;
                    $resp = array('status' => 200, 'message' =>  'Nama & Umur Tidak bisa kosong');
                } else {
                    $respStatus = 400;
                    $resp = $this->KaryawanModel->insert_karyawan($params);
                }
                json_output($respStatus, $resp);
            }
        }
    }

    public function update_karyawan($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
            json_output(200, array('status' => 200, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->KaryawanModel->check_auth_client();
            if ($check_auth_client == true) {
                parse_str(file_get_contents('php://input'), $_PUT);
                $params = $_PUT;
                if ($params['nama'] == "" || $params['umur'] == "") {
                    $respStatus = 400;
                    $resp = array('status' => 200, 'message' =>  'Nama & Umur Tidak bisa kosong');
                } else {
                    $respStatus = 400;
                    $resp = $this->KaryawanModel->update_karyawan($id, $params);
                }
                json_output($respStatus, $resp);
            }
        }
    }

    public function delete_karyawan($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'DELETE' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
            json_output(200, array('status' => 200, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->KaryawanModel->check_auth_client();
            if ($check_auth_client == true) {
                $resp = $this->KaryawanModel->delete_karyawan($id);
                json_output(200, $resp);
            }
        }
    }
}
