<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KaryawanModel extends CI_Model {

    var $client_service = "contoh-client";
    var $auth_key       = "contohapi";

    public function check_auth_client()
    {
        $client_service = $this->input->get_request_header('Client-Service', TRUE);
        $auth_key  = $this->input->get_request_header('Auth-Key', TRUE);

        if ($client_service == $this->client_service && $auth_key == $this->auth_key) {
            return true;
        } else {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized.'));
        }
    }

    public function get_all_karyawan() {
        return $this->db->get('karyawan')->result();
    }

    public function get_karyawan($id)
    {
        return $this->db->get_where('karyawan', array('id_karyawan =' => $id))->row();
    }

    public function insert_karyawan($data)
    {
        $this->db->insert('karyawan', $data);
        return array('status' => 201, 'message' => 'Data Karyawan Berhasil Ditambah.');
    }

    public function update_karyawan($id, $data)
    {
        $this->db->where('id_karyawan', $id)->update('karyawan', $data);
        return array('status' => 200, 'message' => 'Data Karyawan Berhasil Diubah.');
    }

    public function delete_karyawan($id)
    {
        $this->db->where('id_karyawan', $id)->delete('karyawan');
        return array('status' => 200, 'message' => 'Data Karyawan Berhasil Dihapus.');
    }

}